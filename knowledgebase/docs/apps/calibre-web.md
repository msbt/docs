# <img src="/img/calibre-web-logo.png" width="25px"> Calibre Web App

## About

Calibre Web is a web app for browsing, reading and downloading eBooks stored in a Calibre database.

* Questions? Ask in the [Cloudron Forum - Calibre Web](https://forum.cloudron.io/category/105/calibre)
* [Calibre Web Website](https://github.com/janeczku/calibre-web)
* [Calibre Web issue tracker](https://github.com/janeczku/calibre-web/issues)

## Importing existing calibre database

To import an existing Calibre database, do the following:

* Stop the app
* Copy the database into `/app/data/library` using the File Manager
* Start the app

