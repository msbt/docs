# <img src="/img/sogo-logo.png" width="25px"> SOGo App

## About

SOGo is a fully supported and trusted groupware server with a focus on scalability and open standards.

* Questions? Ask in the [Cloudron Forum - SOGo](https://forum.cloudron.io/category/58/sogo)
* [SOGo Website](http://www.sogo.nu/)
* [SOGo support](https://sogo.nu/support.html)
* [SOGo docs](https://sogo.nu/support.html#/documentation)
* [SOGo issue tracker](https://sogo.nu/support.html#/bugs)

## Login

SOGo only works with mailbox accounts on the Cloudron. Login using the full email address including the domain as the username.

## Identities

SOGo is setup out of the box as an email client for Cloudron for all mailboxes on the Cloudron.

The full name is set from the Cloudron user profile. To change the name, you must change the name
in Cloudron dashboard and restart SOGo. Without restarting SOGo, the name change does not get picked
up by SOGo since it appears to cache the value in memory.

To add an identity, go to `Preferences` -> `Mail` -> `IMAP Accounts` -> `New identity`:

<center>
<img src="/img/sogo-email-identity-setup.png" class="shadow" width="100%">
</center>

By default, user's cannot change their full name inside SOGo. To allow users to change their name,
change `SOGoMailCustomFromEnabled` to `YES` in `/app/data/sogo.conf` using the [File manager](/apps/#file-manager)
and restart the app. Please note that this variable already exists in the config file and creating
duplicate entries will cause SOGo to not start up.

## External domains

The SOGo app does not support adding domains that are not managed in Cloudron.
Consider using the [rainloop app](/apps/rainloop) as an alternative.

### Sieve Scripts

SOGo UI only supports setting up a limited set of filtering rules. You can setup more advanced rules
using the Rainloop or Roundcube app.

## CalDAV

SOGo supports syncing using CalDAV:

Clicking on the 'ribbon' next to the calendar shows a popup menu.

<center>
<img src="/img/sogo-links-to-calendar.png" class="shadow" width="50%">
</center>

Clicking on `Links to this Calendar` will show the calendar settings for various clients.

<center>
<img src="/img/sogo-calendar-links.png" class="shadow" width="50%">
</center>

!!! note "Calendar URLs"
    CalDAV URL - https://sogo.example.com/SOGo/dav/<username>/Calendar/personal/
    <br>
    Wedav ICS URL - https://sogo.example.com/SOGo/dav/<username>/Calendar/personal.ics
    <br>
    WebDAV XML URL - https://sogo.example.com/SOGo/dav/<username>/Calendar/personal.xml

## CardDAV

Clicking on the 'ribbon' next to the address book shows a popup menu.

<center>
<img src="/img/sogo-links-to-address-book.png" class="shadow" width="50%">
</center>

Clicking on `Links to this Address book` will show the address book settings for various clients.

<center>
<img src="/img/sogo-address-book-links.png" class="shadow" width="50%">
</center>

!!! note "Address book URLs"
    CardDAV URL - https://sogo.example.com/SOGo/dav/<username>/Contacts/personal/

## ActiveSync

Exchange ActiveSync is a protocol used by Microsoft Exchange to sync mobile devices.
The Cloudron SOGo app does not support ActiveSync. It only supports clients that
use IMAP/SMTP/CardDAV/CalDAV.

## UI Issues

SOGo behaves differently depending on how you access the app.
If you navigate to SOGo by clicking on the icon on your Cloudron
dashboard, parts of the SOGo UI [do not work](https://sogo.nu/bugs/view.php?id=3900).

This issue manifests itself as:

* Email delete button not working
* Compose email popup not closing. Sometimes, it ends up closing the tab itself.
* The browser's web inspector console displays a DOMException with the message
`"Permission denied to access property \"$mailboxController\" on cross-origin object"`.

To workaround this, always use SOGo by opening a new browser tab and entering the
SOGo domain name directly.

## CalDAV and CardDAV Migration

Follow [this guide](https://blog.cloudron.io/carddav-and-caldav-migration/)
to migrate CardDAV and CalDAV resources to and from existing installations.

## Mark as spam

To mark emails as spam (or ham), click on the gravatar icon in the email header.
Then, there is a thumbs down icon for marking as spam.

