# <img src="/img/invoiceninja-logo.png" width="25px"> Invoice Ninja App

## About

InvoiceNinja is the leading self-host platform to create invoices, accept payments, track expenses & time tasks. Support WePay, Stripe, Braintree, PayPal, Zapier, and more!

* Questions? Ask in the [Cloudron Forum - Invoice Ninja](https://forum.cloudron.io/category/11/invoice-ninja)
* [Invoice Ninja Website](https://www.invoiceninja.org)
* [Invoice Ninja forum](https://forum.invoiceninja.com/)

## Customizations

InvoiceNinja customizations can be made by opening a  [File Manager](/apps#file-manager)
and editing `/app/data/env`.

## Invoices

The app is pre-configured to send any invoices every 6 hours. To run the invoice
sending task manually, open a [Web terminal](/apps#web-terminal) and run:

```
sudo -u www-data /usr/local/bin/php /app/code/artisan ninja:send-invoices
```

## Reminders

The app is pre-configured to send out reminders at 0800 UTC. This limitation
is because running the send-reminders cron more than once a day would
[cause problems](https://github.com/invoiceninja/invoiceninja/issues/1921#issuecomment-368806883)
(duplicate late fees, etc.).

To run the reminders task manually, open a [Web terminal](/apps#web-terminal) and run:

```
sudo -u www-data /usr/local/bin/php /app/code/artisan ninja:send-reminders
```

## Cron logs

Cron logs are stored in `/app/data/storage/logs/cron.log`.

## API Key

To get the API key, open `/app/data/.env` with the [File manager](/apps#file-manager), and lookg
for `API_SECRET`.

