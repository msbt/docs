# <img src="/img/traccar-logo.png" width="25px"> Traccar App

## About

Traccar is an open source GPS tracking system.

* Questions? Ask in the [Cloudron Forum - Traccar](https://forum.cloudron.io/category/146/traccar)
* [Traccar Website](https://traccar.org)
* [Traccar docs](https://www.traccar.org/documentation/)
* [Traccar forums](https://www.traccar.org/forums/)
* [Traccar issue tracker](https://github.com/traccar/traccar/issues/)

## Admin Password

If you forgot the admin password, it [cannot be reset by email](https://www.traccar.org/forums/topic/admin-pass-reset/).

From the comment [here](https://github.com/traccar/traccar/issues/1293#issuecomment-121156068), you can reset the password to `password` by updating the database directly.

Open a [Web terminal](/apps/#web-terminal) and click the PostgreSQL button on the top bar and click enter. In the prompt,
execute

```
dbb969ebe64f58473ebed981bb1b69d64c=> UPDATE tc_users SET hashedpassword='ef38a22ac8e75f7f3a6212dbfe05273365333ef53e34c14c', salt='000000000000000000000000000000000000000000000000' WHERE name='admin';
UPDATE 1
```

## Custom config

Traccar provides a wide variety of [configuration](https://www.traccar.org/configuration-file/) options like:

* [Notifications](https://www.traccar.org/documentation/notifications/)
* [SMS Service](https://www.traccar.org/http-sms-api/)
* [Reverse Geocoding](https://www.traccar.org/reverse-geocoding/)

They can be placed in `/app/data/traccar.xml` using the [File manager](/apps/#file-manager).

## Registration

Registration is disabled by default. This can be enabled in `Settings` -> `Server` -> `Permissions` -> `Registration`.

## Ports for Devices

Traccar can work with a large variety of devices and protocols but often the ports required for this are hardcoded. A full list of devices and ports can be found [here](https://www.traccar.org/devices/).
If further ports are required, please let us know in the [forum](https://forum.cloudron.io/category/146/traccar) and we will add those.
