# <img src="/img/kimai-logo.png" width="25px"> Kimai App

## About

Kimai is a free & open source timetracker.

* Questions? Ask in the [Cloudron Forum - Kimai](https://forum.cloudron.io/category/45/kimai)
* [Kimai Website](https://www.kimai.org/)
* [Kimai issue tracker](https://github.com/kevinpapst/kimai2/issues)

## Plugins

Kimai2 supports both free and paid plugins or sometimes called bundles.
The [marketplace](https://www.kimai.org/store/) offers a selection of mostly paid plugins, while there are many free ones on github. Plugins can be obtained either through git checkout or downloading and extracting a zip bundle into `/app/data/plugins`.

The following example installes the demo plugin using the [Web terminal](/apps#web-terminal) into the running app instance:

```
cd /app/data/plugins
git clone https://github.com/Keleo/DemoBundle.git
chown -R www-data.www-data .
cd /app/code
sudo -u www-data bin/console kimai:reload
```

## Customization

Use the [File Manager](/apps#file-manager) to edit custom configuration under `/app/data/local.yaml`.

See [Kimai customization docs](https://www.kimai.org/documentation/configurations.html) for more information.

Once the `local.yaml` is updated, restart the app from the Cloudron dashboard.

