# <img src="/img/gogs-logo.png" width="25px"> Gogs App

## About

Gogs is a painless self-hosted Git service.

* Questions? Ask in the [Cloudron Forum - Gogs](https://forum.cloudron.io/category/70/gogs)
* [Gogs Website](https://gogs.io)
* [Gogs issue tracker](https://github.com/gogs/gogs/issues)

## Customizing Gogs

Gogs supports various [customizations](https://gogs.io/docs/advanced/configuration_cheat_sheet).
To add customizations, use the [File Manager](/apps#file-manager)) and
edit the file named `/app/data/app.ini`.

After editing, restart the app for the changes to take effect.

