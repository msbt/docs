# <img src="/img/cryptpad-logo.png" width="25px"> CryptPad App

## About

CryptPad is an end-to-end encrypted and open-source collaboration suite.

* Questions? Ask in the [Cloudron Forum - CryptPad](https://forum.cloudron.io/category/142/cryptpad)
* [CryptPad Website](https://cryptpad.fr/)
* [CryptPad docs](https://docs.cryptpad.fr/en/)
* [CryptPad issue tracker](https://github.com/xwiki-labs/cryptpad/issues)

## Admin

To make a user an admin:

* Go to `/settings/#account` and copy the Public Signing Key.
* Edit `/app/data/config.js` using [File manager](/apps/#file-manager) and put the key in the `adminKeys` field.
* Restart the app

## Disable registration

Registration can be disabled in the `Administration` section.

<center>
<img src="/img/cryptpad-close-registration.png" class="shadow" width="500px">
</center>

## Checkup

Visit `/checkup` of your installation to run through the CryptPad checklist.

