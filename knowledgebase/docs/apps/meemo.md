# <img src="/img/meemo-logo.png" width="25px"> Meemo App

## About

Meemo simplifies your life by capturing what's on your mind. From short lists, ideas, links, inspiration, to lengthy research, pictures and memories. 

* Questions? Ask in the [Cloudron Forum - Meemo](https://forum.cloudron.io/category/35/meemo)
* [Meemo Website](https://meemo.minimal-space.de/)

## Chrome extension

The [Meemo chrome extension](https://chrome.google.com/webstore/detail/meemo/lhkjapedcimeeiildlmebipnekekjmod)
allows to add the current page with notes into Meemo.

