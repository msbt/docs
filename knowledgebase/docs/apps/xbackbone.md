# <img src="/img/xbackbone-logo.png" width="25px"> XBackBone App

## About

XBackBone is a simple and lightweight PHP file manager that support the instant sharing tool ShareX and UNIX systems.

* Questions? Ask in the [Cloudron Forum - XBackBone](https://forum.cloudron.io/category/147/xbackbone)
* [XBackBone Website](https://xbackbone.app/)
* [XBackBone forum](https://github.com/SergiX44/XBackBone/discussions)
* [XBackBone issue tracker](https://github.com/SergiX44/XBackBone/issues)

## Registration

Registration is disabled by default. This can be enabled in `System` -> `System Settings`.

