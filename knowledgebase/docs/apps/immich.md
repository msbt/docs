# <img src="/img/immich-logo.png" width="25px"> Immich App

## About

Immich is a high performance self-hosted photo and video backup solution.

* Questions? Ask in the [Cloudron Forum - Immich](https://forum.cloudron.io/category/151/immich)
* [Immich Website](https://www.immich.app/)
* [Immich issue tracker](https://github.com/immich-app/immich/issues)
