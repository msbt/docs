# Running Laravel Apps
  
## Overview

In this guide, we will see how to run Laravel apps on Cloudron using the LAMP stack.

## Install LAMP

First, install the [LAMP app](https://www.cloudron.io/store/lamp.cloudronapp.php74.html) on
Cloudron.

## Increase memory limit

As the next step, bump the [memory limit](/apps/#memory-limit) of the LAMP app to 1GB. This is required for
composer to run reliably.

## Create Laravel Project

Open a [Web Terminal](/apps/#web-terminal) and create a Laravel app using composer. We switch to `www-data` user
because the web server runs as that user. Then, we switch the directory to `/app/data` and use composer to create
a empty Laravel project.

```
root@7c29d3b7-b93d-4c75-932e-c771a7383e39:/app/code# su - www-data
www-data@7c29d3b7-b93d-4c75-932e-c771a7383e39:~$ cd /app/data
www-data@7c29d3b7-b93d-4c75-932e-c771a7383e39:~$ composer create-project laravel/laravel my-project
Creating a "laravel/laravel" project at "./my-project"
Installing laravel/laravel (v8.5.18)
  - Installing laravel/laravel (v8.5.18): Downloading (100%)         
Created project in /app/data/my-project
> @php -r "file_exists('.env') || copy('.env.example', '.env');"
Loading composer repositories with package information
Warning from https://repo.packagist.org: You are using an outdated version of Composer. Composer 2 is now available and you should upgrade. See https://getcomposer.org/2
Updating dependencies (including require-dev)
Package operations: 104 installs, 0 updates, 0 removals
  - Installing voku/portable-ascii (1.5.6): Downloading (100%)         
  - Installing symfony/polyfill-php80 (v1.22.1): Downloading (100%)         
  - Installing symfony/polyfill-mbstring (v1.22.1): Downloading (100%)         
  - Installing symfony/polyfill-ctype (v1.22.1): Downloading (100%)         
  - Installing phpoption/phpoption (1.7.5): Downloading (100%)         
....
> @php artisan key:generate --ansi
Application key set successfully.
```

## Configure Apache

By default, Apache is configured to serve the `/app/data/public` directory. The Laravel public directory is however located
under `/app/data/my-project/public`. To configure Apache, use the [File Manager](/apps/#file-manager) and edit `/app/data/apache/app.conf`.
After saving the config, restart the app for the changes to take effect.

```
ServerName %{HTTP_HOST}

<VirtualHost *:80>
    DocumentRoot /app/data/my-project/public

    LogFormat "%{X-Forwarded-For}i %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" proxy
    CustomLog "|/bin/cat" proxy
    ErrorLog "|/bin/cat"

    <Directory /app/data/my-project/public>
        Options +FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    # Do not remove this include. It's required for your app to see the Real IP
    Include "/app/code/apache/rpaf.conf"
    # This line can be commented out, if you do no require PHPMyAdmin Access
    Include "/app/code/apache/phpmyadmin.conf"

</VirtualHost>
```

After app restart, you should see the default Laravel page:

<img src="/guides/img/lamp-laravel.png" class="shadow" width="500px">

## Configure Queue Worker

To configure Laravel Queue worker, edit `/app/data/run.sh` with the following line and restart the app:

```
sudo -u www-data /usr/bin/php /app/data/my-project/artisan queue:work --queue=high,standard,low --sleep=3 --tries=3 &
```

## Cron Jobs

Laravel also depends on a scheduler for tasks scheduled for later. Those can be run via cron.
Add the following job into the crontab as mentioned in the [cron](https://docs.cloudron.io/apps/lamp/#cron-support) docs:

```
* * * * * cd /app/data/my-project/ && sudo -u www-data /usr/bin/php /app/data/my-project/artisan schedule:run
```
