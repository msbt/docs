# Decrypt Backups

## Overview

Cloudron supports encryption of backups with a passphrase. In this guide, we will see
how to decrypt Cloudron backups on your laptop/PC.

## Prerequisites

Have your PC/laptop setup with the [Cloudron CLI tool](/custom-apps/cli/). You can do this
by `sudo npm install -g cloudron`.

Then, login using `cloudron login my.example.com`.

## Download backup

This guide assumes you have the backup downloaded locally. If not, see [this guide](/guides/download-backups/).

## Decrypt

### tgz

If the backup format is tgz, the download will be a single file with the extension `.tar.gz.enc`.

```
$ cloudron backup decrypt --password=passphrase backupid.tar.gz.enc > backupid.tar.gz
$ tar zxvf backupid.tar.gz
```

### rsync

If the backup format is rsync, the download will just be a directory.

```
$ mkdir /path/to/unencrypted
$ cloudron backup decrypt-dir --password=passphrase /path/to/encrypted /path/to/unencrypted
```

