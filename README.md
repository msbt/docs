This repository holds the Cloudron documentation, knowledge base and rest api docs

Live docs site: [https://docs.cloudron.io](https://docs.cloudron.io)

## Prerequisites

* mkdocs
* mkdocs-material
* mkdocs-redirects 
* redoc-cli

Just run ./update.sh it will install the dependencies

## Preview

```
cd knowledgebase
mkdocs serve
```

## Build

```
mkdocs Build
````

## API Docs

```
npx @redocly/cli preview-docs  cloudron_api_swagger.yaml 
```
